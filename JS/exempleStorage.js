/**
 * @author Johnny Tsheke
 */
//trim() enleve les espaces blancs au debut et a la fin de la chaine
function connecter()
{
var login=$(':input[name="login"]').eq(0).val().trim();
var pass=$(':input[name="motDePasse"]').eq(0).val().trim();

var savedPass=$.localStorage(login);//lecture localStorage

if(pass==savedPass)
{ 
	return(true);//mot de passe correct. on autorise la connexion
}
var mesg="Mot de passe incorrect pour "+login+" ";
$("p.affiche").eq(0).html(mesg);
return(false);	
}

function enregistrer()
{
 var login=$(':input[name="slogin"]').eq(0).val().trim()+"";
 var pass=$(':input[name="smotDePasse"]').eq(0).val().trim();
 if(login.length==0||pass.length==0)
 {
 	$("p.affiche").eq(1).prepend("<br>Entrer un login et un mot de passe svp!");
 }
 else{
 $.localStorage(login,pass);//le storage a pour nom login et paur valeur mot de passe
 var aff="<br> Mot de passe enregistre pour "+login+" ";
 $("p.affiche").eq(1).prepend(aff);
 $('form').eq(1).find('input[name="slogin"], input[name="smotDePasse"]').val("");
 }
 return(false);	
}

$(document).ready(function(){
	$('form').eq(0).submit(function(){
		return(connecter());
	});
	$('form').eq(1).submit(function(){
		return(enregistrer());
	});
});