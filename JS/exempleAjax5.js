/**
 * @author Johnny Tsheke
 */

function initDatatable(ajaxurl){
	// création datatable
	$("#univtab").show();
	$("#univtab").DataTable({
		"processing":true,
		ajax:"../PHP4AJAX/exempleAjax5.php?search=canada",
		columns:[
		{data:"nom"},
		{data:"ville"},
		{data:"pays"},
		{data:"image"}
		]
	    
	});
}

function reloadDatatable(ajaxurl){
	ajaxurl= ajaxurl.trim();
	if ( ! $.fn.DataTable.isDataTable( '#univtab' ) ) {
		// appeler la fonction pour créer datatable
		initDatatable(ajaxurl);
		}else{//recharger les données
			$("#univtab").DataTable().ajax.url( ajaxurl ).load();
		}
		
}

$(document).ready(function(){
	$("#univtab").hide();
	$("#universites").click(function(){
		var ajaxurltoutes ="../PHP4AJAX/exempleAjax5.php";
		reloadDatatable(ajaxurltoutes)
	});
	
	$("#canada").click(function(){
		// seulement universités du Canada
		var ajaxurlcanada ="../PHP4AJAX/exempleAjax5.php?search=canada";
		reloadDatatable(ajaxurlcanada);
	});
	$("#autres").click(function(){
		// seulement universités des autres pays que le Canada
		var ajaxurlautres ="../PHP4AJAX/exempleAjax5.php?search=autres";
		reloadDatatable(ajaxurlautres);
	});
});
