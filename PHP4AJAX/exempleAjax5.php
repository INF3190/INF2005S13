<?php
/* 
 * Cet exemple montre comment lire un fichier json en PHP
 * images https://pixabay.com
 */

$jsondata = file_get_contents('../JS/exempleAjax5.json');
//https://www.php.net/manual/en/function.file-get-contents.php

// echo $jsondata;
$jsonDecoded = json_decode($jsondata,true); // table
//$jsonDecoded = json_decode($jsondata); // objet
//https://www.php.net/manual/en/function.json-decode.php

//print_r($jsonDecoded);

function createImgTag($records){ // creation url img a partir du chemin fichier image
    $recordsWithImg= array();
    foreach ($records as $record){
        $record['image'] ="<img src='".$record['image']."' style='width:100%;'/></div>";
        $recordsWithImg[] = $record;
    }
    return ($recordsWithImg);
}

$returnval= array();
if(!isset($_REQUEST['search'])){
    $returnval = $jsonDecoded;
    
}elseif (strcasecmp ($_REQUEST['search'],'Canada') == 0){
    //retorune universités du canada
    foreach ($jsonDecoded as $universite){
        if(strcasecmp($universite['pays'],'Canada') == 0){
            $returnval[] = $universite;
        }
    }
}else{
    // Les autres universités
    foreach ($jsonDecoded as $universite){
        if(strcasecmp($universite['pays'],'Canada') != 0){
            $returnval[] = $universite;
        }
    }
}

$returnval = createImgTag($returnval);// pour créer les tag img
$returnval = array('data'=>$returnval);
echo json_encode($returnval,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
// afiche json. ce qui retourne json si appel ajax. 