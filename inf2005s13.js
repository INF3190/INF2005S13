/**
 * Application nodejs pour la séance 13 du cours INF2005 @UQAM
 * node inf2005s13.js
 */
var express = require('express');
var app=express();
var bodyParser = require('body-parser');
var info=require('./JS/backend4ajax');
app.use(express.static(__dirname));
app.get('/infocours', function(reg,res){
	console.log(info.infocours());
	res.json(info.infocours());
	//res.send("merci");
});
var portNumber=3000;// au besoin changez le numero de port
app.listen(portNumber,function(){console.log(' le serveur fonctionne sur le port: '+portNumber)});
console.log('serveur demarré avec success');