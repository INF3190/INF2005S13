/**
 * @author Johnny Tsheke
 */

$(document).ready(function(){
	$("#universites").click(function(){
		$("#univtab").show();
		$("#univtab").DataTable({
			"processing":true,
			ajax:"../JS/exempleAjax4.json",
			columns:[
			{data:"nom"},
			{data:"Ville"},
			{data:"Pays"}
			]
		    
		});
	});
});
